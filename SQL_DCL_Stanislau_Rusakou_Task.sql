CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

--- 2

GRANT SELECT ON TABLE customer TO rentaluser;

--- 3

CREATE ROLE rental;

GRANT rental TO rentaluser;

--- 4


GRANT SELECT, INSERT, UPDATE ON TABLE rental TO rental;

GRANT USAGE, UPDATE ON SEQUENCE rental_rental_id_seq TO rental;

--- performed as rentaluser
UPDATE rental
SET return_date = CURRENT_TIMESTAMP, last_update = CURRENT_TIMESTAMP
WHERE rental_id = 1;

---5

REVOKE INSERT ON TABLE rental FROM rental;